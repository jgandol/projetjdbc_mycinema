package presentation;

import dto.ActeurDTO;
import dto.GenreDTO;
import metier.DAOMetier;
import metier.IDAOMetier;
import metier.Navigation_Acteur;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.List;

public class CineActeur {
    private Navigation_Acteur cinema;
    private ActeurDTO acteur = null;
    private int nacteur =0;

    private JPanel rootPanel;
    private JTextField textFieldNom;
    private JLabel labelNom;
    private JLabel labelPrenom;
    private JTextField textFieldPrenom;
    private JButton buttonCarriere;
    private JButton buttonGaucheMax;
    private JButton buttonGauche;
    private JButton buttonDroit;
    private JButton buttonDroitMax;
    private JProgressBar progressBarActeur;
    private JPanel j2;
    private JPanel j1;
    private JPanel j3;

    private Color blanc = new Color(0xf5f0f6);
    private Color bleu = new Color(0x9da9a0);
    private Color bleufonce = new Color(0x2b4162);
    private Color marron = new Color(0xd7b377);
    private Color marronfonce = new Color(0xd8f754f);
    private Color gris = new Color(0xc0caad);

    private void init() throws SQLException {
        this.buttonGaucheMax.setIcon(new javax.swing.ImageIcon("/home/jeremiepop/Images/icones/boutons/first.GIF"));
        this.buttonGauche.setIcon(new javax.swing.ImageIcon("/home/jeremiepop/Images/icones/boutons/previous.GIF"));
        this.buttonDroit.setIcon(new javax.swing.ImageIcon("/home/jeremiepop/Images/icones/boutons/next.GIF"));
        this.buttonDroitMax.setIcon(new javax.swing.ImageIcon("/home/jeremiepop/Images/icones/boutons/last.GIF"));

        this.j1.setBackground(gris);
        this.j2.setBackground(gris);
        this.j2.setBackground(gris);
        this.rootPanel.setBackground(gris);
        this.labelNom.setForeground(blanc);
        this.labelPrenom.setForeground(blanc);
        this.textFieldNom.setBackground(bleu);
        this.textFieldNom.setForeground(bleufonce);
        this.textFieldPrenom.setBackground(bleu);
        this.textFieldPrenom.setForeground(bleufonce);
        this.buttonDroit.setBackground(blanc);
        this.buttonGaucheMax.setBackground(blanc);
        this.buttonGauche.setBackground(blanc);
        this.buttonDroitMax.setBackground(blanc);
        this.buttonCarriere.setBackground(blanc);
        this.progressBarActeur.setBackground(marron);
        this.progressBarActeur.setForeground(marronfonce);


            //lien avec la BDD
            this.cinema = new Navigation_Acteur();
            this.progressBarActeur.setMaximum(cinema.getNbActeurs());
            acteur = cinema.getFirstActeur();
            infoActeurBD();


    }

    public CineActeur() throws SQLException {
        init();

        buttonGaucheMax.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                try{
                    acteur=cinema.getFirstActeur();
                } catch (Exception e){
                    System.err.println(e.getMessage());
                }
                if(acteur!=null)
                    infoActeurBD();
            }
        });
        buttonGauche.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                try{
                    acteur=cinema.getPreviousActeur();
                } catch (Exception e){
                    System.err.println(e.getMessage());
                }
                if(acteur!=null)
                    infoActeurBD();
            }
        });
        buttonDroit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                try{
                    acteur=cinema.getNextActeur();
                } catch (Exception e){
                    System.err.println(e.getMessage());
                }
                if(acteur!=null)
                    infoActeurBD();
            }
        });
        buttonDroitMax.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                try{
                    acteur=cinema.getLastActeur();
                } catch (Exception e){
                    System.err.println(e.getMessage());
                }
                if(acteur!=null){
                    infoActeurBD();
                    buttonGauche.setEnabled(true);
                } else {
                    buttonDroitMax.setEnabled(false);
                }

            }
        });
    }

    private void infoActeurBD(){
        //On recupere les infos de l'acteur courant
        // et affichage des informations
        try{
            nacteur = acteur.getNacteur();
            this.textFieldNom.setText(acteur.getNom());
            this.textFieldPrenom.setText(acteur.getPrenom());

            // Maj de la progress Bar
            this.progressBarActeur.setValue(cinema.position());
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    public static void main(String[] args) throws SQLException {
        JFrame frame = new JFrame("CineActeur");
        frame.setContentPane(new CineActeur().rootPanel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}
