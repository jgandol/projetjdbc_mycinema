package presentation;

import dto.FilmDTO;
import dto.GenreDTO;
import java.awt.*;
import metier.DAOMetier;
import metier.IDAOMetier;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.util.List;

public class FormFilm {
    private IDAOMetier cinema;

    private JPanel rootPanel;
    private JList<GenreDTO> listGenre;
    private JList<FilmDTO> listFilmDuGenre;
    private JTextArea textAreaNbFilmsDuGenre;
    private JTextArea textAreaInfosFilm;

    private Color vertkaki = new Color(0x96be8c);
    private Color vertfonce = new Color(0x629460);
    private Color vertnwar = new Color(0x243119);
    private Color vertclair = new Color(0xc9f2c7);



    public FormFilm(){
        init();

        listGenre.addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent listSelectionEvent) {
                try{
                    //Recuperation de la legende selectionnée
                    GenreDTO selection = (GenreDTO) listGenre.getSelectedValue();
                    //reaction 1 --> maj du texarea
                    if(selection == null){
                        textAreaInfosFilm.setText("");
                    } else {
                        textAreaNbFilmsDuGenre.setText("Nombre de films du genre " + selection + " : " + cinema.nbreFilmsDuGenre(selection.getNgenre()));
                    }
                    //reaction 2 --> maj du text areaRealisateur


                    //reaction 3 --> maj de JlistFIlmDuGenre
                    final List<FilmDTO> films = cinema.ensFilmsDuGenre(selection.getNgenre());
                    ListModel<FilmDTO> lm = new AbstractListModel() {
                        public int getSize() {return films.size();}
                        public FilmDTO getElementAt(int index){return films.get(index);}
                    };

                    listFilmDuGenre.setModel(lm);
                } catch (Exception e) {
                    System.err.println(e.getMessage());
                }



            }
        });

        listFilmDuGenre.addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent listSelectionEvent) {
                FilmDTO selection = listFilmDuGenre.getSelectedValue();
                if(selection != null){
                    textAreaInfosFilm.setText("Acteur principal : " + selection.getNom_acteur());
                    textAreaInfosFilm.append("\nRéalisateur : " + selection.getNom_realisateur());
                } else textAreaInfosFilm.setText("");

            }
        });
    }

    private void init(){
        this.rootPanel.setBackground(vertfonce);
        this.textAreaNbFilmsDuGenre.setBackground(vertkaki);
        this.textAreaInfosFilm.setBackground(vertkaki);
        this.listGenre.setBackground(vertkaki);
        this.listFilmDuGenre.setBackground(vertkaki);
        this.listFilmDuGenre.setForeground(vertnwar);
        this.listGenre.setSelectionBackground(vertclair);
        this.listFilmDuGenre.setSelectionBackground(vertclair);


        try{
            //lien avec la BDD
            cinema = DAOMetier.getInstance();
            final List<GenreDTO> genres = cinema.ensGenres();

            DefaultListModel<GenreDTO> lm = new DefaultListModel<>();
            for (GenreDTO unGenre : genres){
                lm.addElement(unGenre);
            }

            listGenre.setModel(lm);

            this.textAreaNbFilmsDuGenre.setText("Nombre de films du genre");
        } catch (Exception e){
            System.err.println(e.getMessage());
        }
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("FormFilm");
        frame.setContentPane(new FormFilm().rootPanel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}
