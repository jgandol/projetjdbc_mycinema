package metier;

import dto.ActeurDTO;
import dto.FilmDTO;
import dto.GenreDTO;

import java.util.List;
/*
Liste des opérations mises à la disposition de la couche présentation
objectif : faciliter l'evolution de l'application
 */
public interface IDAOMetier {

    List<GenreDTO> ensGenres();

    long nbreFilmsDuGenre(int unGenre);

    List<FilmDTO> ensFilmsDuGenre(int unFilm);

    FilmDTO filmParId(int unFilm);

    //List<ActeurDTO> ensActeurs();

    //List<FilmDTO> ensTitresDunActeur(int unActeur);

}
