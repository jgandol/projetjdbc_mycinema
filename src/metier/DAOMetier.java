package metier;

import dto.FilmDTO;
import dto.GenreDTO;
import service.ConnectCinema;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class DAOMetier implements IDAOMetier {

    // Affiche ngenre et nature de tous les genres
    private final static String SQLfindAllGenres =
            " SELECT ngenre, nature " +
                    " FROM GENRE " +
                    " ORDER BY nature ASC ";

    // Affiche le nombre de films d’un genre spécifié par son ngenre
    private final static String SQLfindNbFilmsByGenre =
            " SELECT COUNT(*) as nbre " +
                    " FROM FILM " +
                    " WHERE ngenre =  ?";

    // Affiche nfilm, titre, nom réalisateur, nom acteur des films d’un genre spécifié par son ngenre
    private final static String SQLfindAllFilmByGenre =
            " SELECT nfilm, titre, realisateur as nom_realisateur, a.nom as nom_acteur " +
                    " FROM FILM f " +
                    " INNER JOIN ACTEUR a	ON f.nacteurPrincipal = a.nacteur" +
                    " WHERE ngenre = ?" +
                    " ORDER BY titre";

    // Affiche nfilm, titre, nom réalisateur, nom acteur d’un film spécifié par son nfilm
    private final static String SQLfindFilmById =
            " SELECT f.nfilm, f.titre, f.realisateur as nom_realisateur, a.nom as nom_acteur" +
                    " FROM ACTEUR a" +
                    " INNER JOIN FILM f ON a.nacteur = f.nacteurPrincipal" +
                    " WHERE f.nfilm = ?";


    @Override
    public List<GenreDTO> ensGenres() {
        List<GenreDTO> liste = new ArrayList<>();
        try{
            Statement instruction = ConnectCinema.getINSTANCE().createStatement();
            ResultSet rs = instruction.executeQuery(SQLfindAllGenres);

            while( rs.next() ){
                int ngenre = rs.getInt(1);
                String nature = rs.getString("NATURE");

                liste.add( new GenreDTO(ngenre, nature));

            }
        } catch (SQLException e){
            e.printStackTrace();
        }
        return liste;
    }

    @Override
    public long nbreFilmsDuGenre(int unGenre) {

        try{
            Connection conn = ConnectCinema.getINSTANCE();
            PreparedStatement instruction = conn.prepareStatement(SQLfindNbFilmsByGenre);
            instruction.setInt(1,unGenre);
            ResultSet rs = instruction.executeQuery();
            if(rs.next()){
                return rs.getLong("nbre");
            }

        } catch (SQLException e){
            e.printStackTrace();
        }
        return -1L;
    }

    @Override
    public List<FilmDTO> ensFilmsDuGenre(int unFilm) {
        List<FilmDTO> liste = new ArrayList<>();
        try{
            Connection conn = ConnectCinema.getINSTANCE();
            PreparedStatement instruction = conn.prepareStatement(SQLfindAllFilmByGenre);
            instruction.setInt(1,unFilm);
            ResultSet rs = instruction.executeQuery();
            // nom film    titre   real    acteur nom
            while( rs.next() ){
                int nFilm = rs.getInt("nfilm");
                String titre = rs.getString("titre");
                String nomReal = rs.getString("nom_realisateur");
                String nomActeur = rs.getString("nom_acteur");
                liste.add( new FilmDTO(nFilm, titre, nomReal, nomActeur));
            }
        } catch (SQLException e){
            e.printStackTrace();
        }
        return liste;
    }

    @Override
    public FilmDTO filmParId(int unFilm) {
        FilmDTO f = null;
        try{
            Connection conn = ConnectCinema.getINSTANCE();
            PreparedStatement instruction = conn.prepareStatement(SQLfindFilmById);
            instruction.setInt(1,unFilm);
            ResultSet rs = instruction.executeQuery();

            if(rs.next()){
                int nFilm = rs.getInt("nfilm");
                String titre = rs.getString("titre");
                String nomReal = rs.getString("nom_realisateur");
                String nomActeur = rs.getString("nom_acteur");

                f = new FilmDTO(nFilm,titre,nomReal,nomActeur);
            }
        } catch (SQLException e){
            e.printStackTrace();
        }
        return f;
    }


    // SINGLETON PRECOCE
    private static DAOMetier INSTANCE = new DAOMetier();
    public static IDAOMetier getInstance() {return INSTANCE;}



    public static void main(String[] args) {
        DAOMetier dao = new DAOMetier();
        System.out.println(dao.ensGenres());
        System.out.println(dao.nbreFilmsDuGenre(6));
        System.out.println(dao.ensFilmsDuGenre(5));
        System.out.println(dao.filmParId(5));
    }
}
