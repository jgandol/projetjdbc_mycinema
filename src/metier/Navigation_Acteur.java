package metier;

import dto.ActeurDTO;
import service.ConnectCinema;

import java.sql.*;

public class Navigation_Acteur {

    private Connection conn = null;
    private Statement stmt = null;
    private ResultSet ars;

    private int nbActeurs = 0;

    private final static String SQLselectedActeurs =
            "SELECT nacteur, a.nom as nom, prenom, " +
                    " naissance, p.nom as nationalite, nbreFilms " +
                    " FROM ACTEUR a " +
                    " INNER JOIN PAYS p " +
                    " ON nationalite = npays " +
                    " ORDER BY a.nom, prenom ";

    public Navigation_Acteur(){
        try {
            ouvrirConnection();
            ars.last();
            nbActeurs = ars.getRow();
            ars.first();
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    public int getNbActeurs() {
        return nbActeurs;
    }

    public int position() throws Exception{
        return ars.getRow();
    }

    public void ouvrirConnection() throws SQLException {
        this.conn = ConnectCinema.getINSTANCE();

        this.stmt = conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_READ_ONLY);

        this.ars = stmt.executeQuery(SQLselectedActeurs);


    }

    public void fermerConnection() throws SQLException {
        if(this.stmt !=null){
            stmt.close();
        }
        if (this.conn != null){
            conn.close();
        }
    }

    public ActeurDTO getFirstActeur() throws SQLException{
        if(ars == null){
            return null;
        }
        ars.first();
        return getActeurDTO();

    }

    public ActeurDTO getNextActeur() throws SQLException{
        if(this.ars == null)
            return null;
        if(this.ars.isLast())
            return null;

        ars.next();
        return getActeurDTO();
    }

    public ActeurDTO getLastActeur() throws SQLException{
        if(this.ars == null)
            return null;

        this.ars.last();

        return getActeurDTO();
    }

    private ActeurDTO getActeurDTO() throws SQLException {
        int nacteur = ars.getInt(1);
        String nom = ars.getString(2);
        String prenom = ars.getString(3);
        Date naissance = ars.getDate(4);
        String nationalite = ars.getString(5);
        int nbreFilms = ars.getInt(6);

        return new ActeurDTO(nacteur,nom,prenom,naissance,nationalite,nbreFilms);
    }

    public ActeurDTO getPreviousActeur() throws SQLException{
        if(this.ars == null)
            return null;
        if(this.ars.isFirst())
            return null;

        ars.previous();

        return getActeurDTO();
    }

    public static void main(String[] args) throws SQLException{
        Navigation_Acteur na = new Navigation_Acteur();

        ActeurDTO acteur = na.getLastActeur();
        while (acteur != null ) {
            System.out.println(acteur);
            acteur = na.getPreviousActeur();
        }

        na.fermerConnection();


    }
}
