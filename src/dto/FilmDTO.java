package dto;

import java.io.Serializable;

public class FilmDTO implements Serializable {
    /////////////////////////// A T T R I B U T E S \\\\\\\\\\\\\\\\\\\\\\\\\\\
    // Class Attributes
    private static final long serialVersionUID = 1L;

    // Instance Attributes
    private int nfilm;
    private String titre;
    private String nom_realisateur;
    private String nom_acteur;

    /////////////////// G E T T E R S    &    S E T T E R S \\\\\\\\\\\\\\\\\\\\

    public int getNfilm() {
        return nfilm;
    }
    public String getTitre() {
        return titre;
    }
    public String getNom_realisateur() {
        return nom_realisateur;
    }
    public String getNom_acteur() {
        return nom_acteur;
    }

    ///////////////////////// C O N S T R U C T O R S \\\\\\\\\\\\\\\\\\\\\\\\\\

    public FilmDTO(int nfilm, String titre, String nom_realisateur, String nom_acteur) {
        super();
        this.nfilm = nfilm;
        this.titre = titre;
        this.nom_realisateur = nom_realisateur;
        this.nom_acteur = nom_acteur;
    }

    ////////////////////// B U S I N E S S    R U L E S \\\\\\\\\\\\\\\\\\\\\\\\


    // toString
    @Override
    public String toString() {
        //return "\n" + nfilm + ":" + titre + " réalisé par " + nom_realisateur + " avec " + nom_acteur;
        return titre;
    }
}
