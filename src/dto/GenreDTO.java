package dto;

import java.io.Serializable;

public class GenreDTO implements Serializable {
    /////////////////////////// A T T R I B U T E S \\\\\\\\\\\\\\\\\\\\\\\\\\\
    // Class Attributes
    private static final long serialVersionUID =1L;

    // Instance Attributes
    private int ngenre;
    private String nature;

    /////////////////// G E T T E R S    &    S E T T E R S \\\\\\\\\\\\\\\\\\\\

    public int getNgenre() {
        return ngenre;
    }
    public String getNature() {
        return nature;
    }

    ///////////////////////// C O N S T R U C T O R S \\\\\\\\\\\\\\\\\\\\\\\\\\

    public GenreDTO(int ngenre, String nature) {
        super();
        this.ngenre = ngenre;
        this.nature = nature;
    }

    ////////////////////// B U S I N E S S    R U L E S \\\\\\\\\\\\\\\\\\\\\\\\



    // toString

    @Override
    public String toString() {
        return  nature;
    }
}
