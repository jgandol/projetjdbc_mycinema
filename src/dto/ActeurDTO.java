package dto;

import java.io.Serializable;
import java.sql.Date;

public class ActeurDTO implements Serializable {
    private static final long serialVersionUID = 1L;

    private int nacteur;
    private String nom;
    private String prenom;
    private Date naissance;
    private String nationalite;
    private int nbreFilms;

    public int getNacteur() {
        return nacteur;
    }
    public String getNom() {
        return nom;
    }
    public String getPrenom() {
        return prenom;
    }
    public Date getNaissance() {
        return naissance;
    }
    public String getNationalite() {
        return nationalite;
    }
    public int getNbreFilms() {
        return nbreFilms;
    }


    public ActeurDTO(int nacteur, String nom, String prenom, Date naissance, String nationalite, int nbreFilms) {
        this.nacteur = nacteur;
        this.nom = nom;
        this.prenom = prenom;
        this.naissance = naissance;
        this.nationalite = nationalite;
        this.nbreFilms = nbreFilms;
    }

    @Override
    public String toString() {
        return "ActeurDTO{" +
                "nacteur=" + nacteur +
                ", nom='" + nom + '\'' +
                ", prenom='" + prenom + '\'' +
                ", naissance=" + naissance +
                ", nationalite='" + nationalite + '\'' +
                ", nbreFilms=" + nbreFilms +
                '}';
    }
}
